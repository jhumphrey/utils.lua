-- the core function is dsvtotable, tsvtotable is jsut a wrapper

-- arguments:
--	raw: the opened file to parse
--	delim: what is the separator 
--	firstrowiskey: the first row provides the keynames for the rest of the table
-- which is treated as an indexed array, the index being the rows
function dsvtotable(raw, delim, firstrowiskeys, keycolum)
	keys={}
	result={}
	if firstrowiskeys then
		-- read a keyline
		keyline = raw:read()
		for k in keyline:gmatch("[^\t]*") do
			table.insert(keys,k)
		end
	end
	-- ok, now for the meat
	for row in raw:lines() do
		entry = {}
		counter=1
		for val in row:gmatch("[^\t]*") do
			if firstrowiskeys then
				entry[keys[counter]] = val
				counter = counter + 1
			else
				table.insert(entry, val)
			end
		end
		if keycolum then
			result[entry[keycolum] or entry[keys[keycolum]]] = entry
		else
			table.insert(result, entry)
		end
	end
	return result
end

function tsvtotable(tsv, firstrowiskeys, keycolum)
	return dsvtotable(tsv, "\t", firstrowiskeys, keycolum)
end
