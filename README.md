This project comes from that weird state where you repeat a lot of utility functions, but don't want to create a whole library.
When finished, this will have a `mkutils` command that will generate a self-contained `utils.lua` file containing all the neccessary utilities from this collection, but no more.

## Already available
None

## Planned

### basics
dumptable()     dumps a lua table in human-readable format

### datatypes
tsvtotable()    convert TSV file to table
dsvtotable()    convert delimiter separated values (tsv, csv, etc) to table

